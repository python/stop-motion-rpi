# -*- coding: utf-8 -*-

from picamera import PiCamera
from gpiozero import Button
from time import sleep

def arret() :
    camera.stop_preview()
    exit()

camera=PiCamera()
button=Button(17,hold_time=1)
button.when_held=arret

camera.rotation=180 #à ajuster ou supprimer au besoin
camera.start_preview()

frame=1

while True :
    button.wait_for_press()
    button.wait_for_release()
    sleep(1)
    camera.capture('/home/pi/Desktop/animation/frame%03d.jpg' % frame)
    frame+=1